package com.nisum.applicant.alex.resource;

import com.nisum.applicant.alex.resource.request.RequestPostUsers;
import com.nisum.applicant.alex.resource.response.ResponseGetUsers;
import com.nisum.applicant.alex.resource.response.ResponsePostUsers;
import com.nisum.applicant.alex.service.UserService;
import com.nisum.applicant.alex.service.dto.UserDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/users")
public class UsersController {

    private final UserService userService;

    public UsersController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePostUsers> save(@RequestBody RequestPostUsers request) {

        UserDto userDto = request.toUser();

        this.userService.userRegister(userDto);

        ResponsePostUsers response = new ResponsePostUsers(userDto);

        return ResponseEntity.ok(response);
    }

    @GetMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ResponseGetUsers>> get() {

        List<UserDto> usersDto = this.userService.getAll();

        List<ResponseGetUsers> response = usersDto.stream()
                .map(ResponseGetUsers::from)
                .collect(Collectors.toList());

        return ResponseEntity.ok(response);
    }

}
