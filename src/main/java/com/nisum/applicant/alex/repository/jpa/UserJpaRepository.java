package com.nisum.applicant.alex.repository.jpa;

import com.nisum.applicant.alex.repository.UserRepository;
import com.nisum.applicant.alex.repository.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<User, String>, UserRepository {


}
